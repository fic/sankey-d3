#Sankey Chart#

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Sankey diagram using D3.js

### How do I get set up? ###

1. `git clone https://bitbucket.org/fic/sankey-d3.git`
2. Ensure that you have [npm](https://www.npmjs.com/) installed .
3. Ensure that you have gulp installed `npm install --global gulp`
4. `npm install --save-dev gulp` (installs dependencies at project level)
5. Ensure that you have [bower](https://bower.io/) installed .
5. `npm install` to install node dependencies
6. `bower install` to install bower dependencies
7. `gulp serve`


### Who do I talk to? ###

* mtmangum@utexas.edu