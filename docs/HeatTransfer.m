%Prompts user to input values for variables
prompt = {'Surface temp. [deg. C]',
          'Initial temp. [deg. C]',
          'Thickness [mm]',
          'Thermal diffusivity [mm^2/s]',
          'Length of simulation [s]',
          'Time step [s]',
          'Number of terms of taylor approximation'};
title = 'Input';
dims = [1 35];
definput = {'100','25','1','8*10^-4','1000','25','1'}; %Default values of variables
answer = inputdlg(prompt,title,dims,definput);

%Convert string inputs to numbers
T1 = str2num(answer{1});
T2 = str2num(answer{2});
L = str2num(answer{3});
alpha = str2num(answer{4});
tfinal = str2num(answer{5});
deltat = str2num(answer{6});
lasttaylorterm = str2num(answer{7});

x = linspace(0,L); %determines x-axis for graph
t = 0; %simulation starts at time zero

while t < tfinal

    %Calculating "true" error function using built-in MATLAB error function
    z = x/(2*(alpha*t)^(1/2)); %defining z for substitution in following line
    T_true = (T1 - T2)*(1-erf(z)) + T2; %Calculate "true" curve

    %Calculating error function term of the approximation curve
    %This is the important part--uses a Taylor series of a user-determined
    %number of terms in order to calculate error function
    for n = 0:lasttaylorterm-1
        if n == 0
            erftaylor(n+1,1:100) = (2/(pi^(1/2)))*z;
        else
            erftaylor(n+1,1:100) = erftaylor(n,1:100) + (2/(pi^(1/2)))*((-1).^n)*(z.^(2*n+1))/(factorial(n)*(2*n+1));
        end
    end
    
    figure(1)
    
    %Calculating the approximation curve using the previously calculated
    %error function term
    for n = 1:lasttaylorterm
        T_approx(1:100,n) = (T1-T2)*(1-erftaylor(n,1:100))+T2;
    end
    
    %Generate plot
    plot(x,T_true,x,T_approx(1:100,lasttaylorterm))
    axis([0 L 0 T1]) %Determine axes
    %This just determines the size of the figure window and where on your 
    %computer screen it appears
    set(gcf,'units','normalized','position',[.02 .08 .95 .8])
    
    %Creates control box to go forward/backward through timeframes
    answer = questdlg('Controls', ...
	'Choose one', ...
	'Previous timeframe','Next timeframe','Abort!','Abort!');

    %Determines what happens when you click a button on the control box
    switch answer
        case 'Previous timeframe'
            if t > deltat
                t = t - deltat; %Move backward one timestep
            else
                t = 0; %If you're already at the beginning of the simulation and click "previous timeframe," nothing happens
            end
        case 'Next timeframe'
            t = t + deltat; %Move forward one timestep
        case 'Abort!'
            t = tfinal; %Aborts simulation
    end

end
