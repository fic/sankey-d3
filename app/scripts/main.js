(function($, d3) {
  'use strict'

  // VARS

  var SCALE = 8e-9;
  var diagram = d3.expandableSankeyDiagram()
                  .scale(SCALE)
                  .on('clickNode', setDetails);


  // ON INIT



  // FUNCIONS

  d3.json("ajax/emissions_sankey_details_2014_adjusted_colours.json", function(error, data) {
   if (error) throw(error);
   console.log(data);
   diagram(d3.select('svg').datum(data));
  });

  var numberFormat0 = d3.format('.1f');

  function numberFormat(x) {
   return numberFormat0(x / 1e9) + ' Gt';
  }

  function setDetails(d) {
   var details = d3.select('#details');
   details.select('h1').text(d.title)
          .append('small').text(numberFormat(d.value));
   details.select('p').text(d.description);

   details.select('tbody')
          .selectAll('tr')
          .remove();

    var rows = details.select('tbody')
                      .selectAll('tr')
                      .data(d.subdivisions)
                      .enter()
                      .append('tr');

   rows.append('td').text(function(d) { return d.label; });
   rows.append('td').text(function(d) { return numberFormat(d.value); });
   rows.append('td').text(function(d) { return d.description; });
  }

})(jQuery, d3)
